'use strict'

var { defineSupportCode } = require('cucumber');
const expect = require('chai').expect;
const R = require('ramda');

defineSupportCode(function ({ setDefaultTimeout, Given, Then, When }) {
    setDefaultTimeout(5000);

    Given(/^an existing user with id (.*)$/, function (userId) {
        //payload should be empty
        this.payload = { };
    });
    
    When(/^I perform the (\w+) against the (.*) endpoint$/, function (verb, endpoint) {
        const that = this;
        return this.doOp(verb, endpoint, this.payload)
        .then(
            res => that.response = res
        );
    });

    Given('a set of existing users', function () {

    })

    Given(/^I want to filter them by (.*)$/, function (filter) {
        this.filter = filter;
    });

    When(/^I perform a (\w+) against the (.*) endpoint, using that filter$/, function (verb, endpoint) {
        const that = this;
        return this.doOp(verb, endpoint + '?filter[' + this.filter + ']=Test country')
        .then(
            res => that.response = R.assoc("body", JSON.parse(res.body), res)
        );
    });

    Then(/^a list of users that match the same (.*)$/, function (filter) {
        //console.log('Response ', this.response.body);
        expect(this.response.body).to.exist;
    });
});
