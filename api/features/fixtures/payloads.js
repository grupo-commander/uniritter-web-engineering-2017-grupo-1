'use strict'

/*
module.exports = {
    "data":
    {
        "type": "users",
        "attributes": {
            "name": "test user 01",
            "email": "testEmail@randomemail.com",
            "avatar": "https://gravatar.com/123",
            "city": "Test City",
            "subdivision": "Test State",
            "country": "Test country"
        }
    }

}
*/

module.exports = {
    'valid user': {
        input: `user: { name: \"John\", email: \"john@gmail.com\" }`,
        output: ` id, name, email`
    }
};
