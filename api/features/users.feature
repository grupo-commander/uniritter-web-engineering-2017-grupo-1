Feature: User mgmt API

   As an API consumer
   I want to be able to manage user data

   Scenario: Add user
   Given a valid user payload
   When I perform an addUser mutation
   Then I receive a 200 status code
   And the payload for the new resource   

   Scenario: Delete user
   Given an existing user
   When I perform a DeleteUser mutation
   Then I receive a 200 status code
   When I perform a POST against the /users endpoint
   Then I receive a 201 status code
   And the payload for the new resource
   And I can GET it by id
   
   Scenario: Delete user
   Given an existing user with id 974636c1-cb4a-483e-8b63-464986a67371
   When I perform the DELETE against the /users/974636c1-cb4a-483e-8b63-464986a67371 endpoint
   Then I receive a 204 status code

   Scenario: Edit user
   Given an existing user
   And I want to update his/her name
   When I perform an EditUser mutation
   Then I receive a 200 status code
   And the user has the new name
   
   Scenario Outline: User listing
   Given a set of existing users
   And I want to filter them by <filter>
   When I perform query against the users collection
   Then I receive a 200 status code
   And a list of users that match the same <filter>

   Examples:
   | filter      |
   | city        |
   | subdivision |
   | country     |
