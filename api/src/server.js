'use strict'

const Hapi = require('hapi');
const require_dir = require('require-directory');
const jwt = require('hapi-auth-jwt2');
const jwksRsa = require('jwks-rsa');
const R = require('ramda');
const {
    graphqlHapi,
    graphiqlHapi
} = require('graphql-server-hapi');

const Server = module.exports = {};

const validateUser = (decoded, request, callback) => {
    // This is a simple check that the `sub` claim
    // exists in the access token. Modify it to suit
    // the needs of your application
    if (decoded && decoded.sub) {
        return callback(null, true);
    }

    return callback(null, false);
}

Server.createServer = function (config, logger, graphqlSchema) {
    logger.debug('Creating server');
    const server = new Hapi.Server();
    server.connection({
        port: config.port,
        router: {
            stripTrailingSlash: true
        }
    });

    return graphqlSchema
    .then(schema => {
        return new Promise((resolve, reject) => {
            server.register([{
                register: jwt
            }, {
                register: require('hapi-cors')
            },{
                register: graphqlHapi,
                options: {
                    path: '/graphql',
                    graphqlOptions: { schema }
                },
            }, {
                register: graphiqlHapi,
                options: {
                    path: '/graphiql',
                    graphiqlOptions: {
                        endpointURL: '/graphql',
                    },
                },
            }],
            (err) => {
                if (err) return reject(err);

                logger.debug('configuring server.', config);

                if (!config.disableAuth) {
                    server.auth.strategy('jwt', 'jwt', 'required', {
                        complete: true,
                        // verify the access token against the
                        // remote Auth0 JWKS 
                        key: jwksRsa.hapiJwt2Key({
                            cache: true,
                            rateLimit: true,
                            jwksRequestsPerMinute: 5,
                            jwksUri: `https://${config.auth0Domain}/.well-known/jwks.json`
                        }),
                        verifyOptions: {
                            audience: 'https://geekrep.com',
                            issuer: `https://${config.auth0Domain}/`,
                            algorithms: ['RS256']
                        },
                        validateFunc: validateUser
                    });
                }

                // todo: refactor for cleaner look
                const resources = R.values(require_dir(module, './resources'));
                R.forEach(r => r.registerRoutes(server, logger), resources);

                return resolve(server);
            })
        })
    })

}
